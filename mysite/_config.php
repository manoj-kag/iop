<?php

global $project;
$project = 'mysite';

global $database;
$database = ''; // Database is defined in _ss_environment.php

require_once('conf/ConfigureFromEnv.php');

// Redirect all requests to HTTPS
if (Director::isLive()) {
    if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off") {
        $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        header('HTTP/1.1 301 Moved Permanently');
        header('Location: ' . $redirect);
        exit();
    }
}

// Set the site locale
i18n::set_locale('fr_FR');

// add to the mysite/_config.php
if (class_exists('MyCustomLeft')) {
    Object::add_extension('LeftAndMain', 'MyCustomLeft');
}

// Desable Ratin by default
Idea::remove_extension('RatingExtension');

// Override theme styles with our custom styles - ?flush=all in cms to see them appear if updated
// @link https://docs.silverstripe.org/en/3/developer_guides/customising_the_admin_interface/typography/
HtmlEditorConfig::get('cms')->setOption('content_css', 'mysite/css/custom-editor.css');

// [JD-2019.07.08] TinyMCE 4 configuration
if (class_exists('CustomHtmlEditorConfig')) {
	/** Example of extension of custom fonts :
	CustomHtmlEditorConfig::get('cms')->setOption('font_formats', CustomHtmlEditorConfig::get('cms')->getOption('font_formats') .'Comfortaa=Comfortaa;');
	*/

	/** Example of extension of custom formats :
	$formats = CustomHtmlEditorConfig::get('cms')->getOption('style_formats');
	$formats[] = [
		'title' => 'Comfortaa',
		'inline' => 'span',
		'classes' => 'Comfortaa',
		'merge_siblings' => true,
	];
	foreach(CustomHtmlEditorConfig::get_available_configs_map() as $key => $title) {
		CustomHtmlEditorConfig::get($key)->setOptions([
			'importcss_append' => true,
			'style_formats' => $formats,
			'style_formats_autohide' => true,
		]);
	}*/

	/** Example of extension of custom colors :
	$colors = [
		"000000", "Black",
		"993300", "Burnt orange",
		"333300", "Dark olive",
		"003300", "Dark green",
		"003366", "Dark azure",
		"000080", "Navy Blue"
	];
	foreach(CustomHtmlEditorConfig::get_available_configs_map() as $key => $title) {
		CustomHtmlEditorConfig::get($key)->setOption('textcolor_map', $colors);
	}*/

	/** Example of extension of custom blocks :
	CustomHtmlEditorConfig::get('cms')->setOption('block_formats', 'Paragraph=p;Heading 3=h3;Heading 4=h4');
	CustomHtmlEditorConfig::get('cms')->setOption('style_formats_merge', false);
	*/
}

// Log all errors by Email to admin
if (Director::isLive()) {
	SS_Log::add_writer(new SS_LogEmailWriter('support@letsco.ovh'), SS_Log::ERR, '<=');
	SS_Log::add_writer(new SS_LogApiWriter('https://logger.letscolab.co/logapi/log'), SS_Log::ERR, '<=');
}
