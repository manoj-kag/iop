<% if File.Link && File.ID > 0 %>
<div class="File uk-margin-large-top">
    <a href="$File.Link" target="_blank" class="btn"><i class="uk-icon-download"></i> $File.Title </a>
</div>
<% end_if %>