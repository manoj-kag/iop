<style type="text/css">
    #$ClassName .pMember {
        margin-top: 4px;
    }

    #$ClassName .pMemberIcon img{
        margin-top: 0px !important;
    }

    #$ClassName .pMember a{
        color: unset;
        font-weight: bold;
    }
    #$ClassName .pSociety {
        color: unset;
    }
    #$ClassName .pDate {
        color: #666666;
        font-size: 14px;
    }

    /*#$ClassName .MainFields img{
        width: 100%;
        border-radius: 1%;
        margin-top: 30px;
    }*/

    #$ClassName .TitleSideBar {
        font-size:23px;
    }
    #$ClassName .pMemberIcon {
        display: inline-block;
        width: 70px;
        overflow: hidden;
        box-shadow: 0 0 8px rgba(0,0,0,0.2);
        border-radius: 50%;
        margin-top: 7px;
        margin-right: 10px;
        margin-left: 16px;
    }

    #$ClassName .pThird {
        font-size: 16px;
        display: inline-block;
        margin-left: 30px;
        line-height: 25px;
        vertical-align: top;
    }

    #$ClassName .Files {
        margin-bottom: 20px;
    }

</style>

<div class="container main-container">
    <% include PreviousNext %>
    
    <div class="uk-grid">

        <% if ListableObject.ResourcesWithSameTags($Top.ID) %>
        <div class="uk-width-medium-3-4">
        <% else %>
        <div class="uk-width-medium-4-4">
        <% end_if %>

            <%-- ----------- MAIN FIELDS ----------- --%>

            <% loop MainFields %>
            <div class="MainFields $Class uk-margin-bottom" data-field="$ID" data-class="$Class" data-id="$Top.ListableObject.ID">
            <% if Top.ExtendedTemplate($ID) %>
                $Top.ListableObject.RenderWith($Top.ExtendedTemplate($ID))
            <% else %>
                <div class="Content">$Content</div>
            <% end_if %>
            </div>
            <% end_loop %>

        </div>

        <%-- ----------- SIDE FIELDS ----------- --%>
        <%-- ----------- LINKED RESOURCES (same tags) ----------- --%>

        <% with ListableObject %>
            <% if ResourcesWithSameTags($Top.ID) %>
                <div class="uk-width-medium-1-4">

                    <div class="Date uk-article-meta uk-margin-bottom">
                        <% _t('CommunityNewsPage.OTHERBLOGS','Contenus similaires dans') %> : <br/>
                        <a href="$Top.Link">$Top.Title</a>
                    </div>

                    <div>
                       <% loop ResourcesWithSameTags($Top.ID, 5) %>
                            <div class="item">
                                $View(vignette)
                            </div>
                        <% end_loop %>
                   </div>
                </div>
            <% end_if %>
        <% end_with %>
    </div>
</div>