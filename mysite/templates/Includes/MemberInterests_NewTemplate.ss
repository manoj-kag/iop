<% if $CurrentMember %>
<div class="uk-margin-large-bottom">
    <% loop Interests %>
      <a class="Interest" href="$Top.Link?interests=$ID" style="display:inline-block;">$Title</a>
    <% end_loop %>
      <% loop $Offers %>
        <a class="Interest" href="$Top.Link?offers=$ID" style="display:inline-block;">$Title</a>
      <% end_loop %>
</div>
<% end_if %>
