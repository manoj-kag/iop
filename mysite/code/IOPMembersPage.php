<?php

/**
 *@since [LDC-2021.06.02] #3356 create matching function between members
 */
class IOPMembersPage extends DataExtension
{
    const MATCHING = 'Matching';
    const PUBLICNAME = 'PublicName';
    protected static $counter = 0;

    private static $db = array(
        'IOPMatchingCount' => 'Int', //[LDC-2021.06.07] #3367 count matching request
        //'IOPMatchingByDefault' => 'Boolean', //[LDC-2021.06.07] #3367 count matching request
    );

    public function updateListFilters(&$filters)
    {
        $filters[IOPMembersPage::MATCHING] = _t('MembersPage.Matching', 'Matching');
    }

    /**
     *@since [LDC-2021.06.07] #3367 show matching members or not by default
     */
    public function updateCMSFields(FieldList $fields){
        $fields->addFieldToTab('Root.Options', new CheckboxField('IOPMatchingByDefault', _t('RegisterForm.IOPMatchingByDefault', 'Matching Actif par défaut ?'))); 
    }

    /**
     *@since [LDC-2021.06.07] #3367 count matching requests
     */
    public function countMatchingRequests(){
        //[LDC-2021.06.07] #3367 count matching request : increment IOPMatchingCount every time the matching function is resqueted

        //TODO : remove the counter, find why this function is called 4x instead of once
        self::$counter++;

        //SS_Log::Log(' COUNTER ' . self::$counter, SS_Log::INFO);

        //incrémenter la colonne "IOPMatchingCount" seuement tous les 4 passages dans la fonction...
        if (self::$counter == 4) {
            //SS_Log::Log(' AVANT ' . $this->owner->IOPMatchingCount, SS_Log::INFO);
            $this->owner->IOPMatchingCount ++;
            $this->owner->write();
            //SS_Log::Log(' APRES EN BDD ' . $this->owner->IOPMatchingCount, SS_Log::INFO);
        }
    }
}

class IOPMembersPage_Controller extends Extension
{
    public function updateFilterForm($key, $form)
    {
        $currentMember = $this->owner->CurrentMember();
        if (IOPMembersPage::MATCHING == $key) {
            if ($currentMember && $currentMember->hasCompleteProfile()){
                $MatchingBtn = new LiteralField($key,'
                    <div class="Matching-btn">
                        <input type="checkbox" name="filters[Matching]" class="checkbox urgent" id="filters_Matching">
                        <label for="filters_Matching"><div class="Matching_label">
                            PROPOSEZ-MOI DES MEMBRES QUI PARTAGENT MA VISION
                        </div></label>
                    </div>');
                $form->Fields()->unshift($MatchingBtn);
            }else{
                $MatchingBtn = new LiteralField($key,'
                    <div class="Matching-btn ProfilNotComplet" title="Seuls les membres ayant un profil complet peuvent activer cette fonction">
                        <input type="checkbox" name="filters[Matching]" class="checkbox urgent" id="filters_Matching">
                        <label for="filters_Matching"><div class="Matching_label">
                            PROPOSEZ-MOI DES MEMBRES QUI PARTAGENT MA VISION
                        </div></label>
                    </div>');
                $form->Fields()->unshift($MatchingBtn);
            }
        }          
    }

    public function updateApplySearchFilters(&$request, $filters)
    {
    	if (!empty($filters[IOPMembersPage::MATCHING])) {
            $currentMember = $this->owner->CurrentMember();
            //SS_Log::Log( ' CURRENT MEMBER ' . $currentMember->Name , SS_Log::INFO);

            if ($currentMember){
                //if current member belong to group 2, do the matching research
                if ($currentMember->hasCompleteProfile()) {
                    if (!$currentMember->IOPMatchingCode){ //in case the member has been inscribe before the creation of the matching function, the profil is complete but he has no IOPMatchingCode
                        $currentMember->GenerateMatchingCode();
                        $currentMember->write();
                    }
                    $listMatchingCode = $this->getMatchingList($currentMember->IOPMatchingCode); 
                    //SS_Log::Log( ' MATCHIN LIST ' . json_encode($listMatchingCode) , SS_Log::INFO);
                   
                    $request = $request->filter('IOPMatchingCode:PartialMatch', $listMatchingCode)->exclude('ID', $currentMember->ID);
                    //SS_Log::Log( ' REQUEST ' .  $request->sql() , SS_Log::INFO);

                    $this->owner->countMatchingRequests();
                }else{
                    //SS_Log::Log( ' member NOT belong to group 2 ' . $currentMember , SS_Log::INFO);
                    //@TODO : $this->owner->SetSessionMessage("Cette fonctionnalité est réservée aux membres ayant entièrement complété leur profil", "bad");
                }   
            }else{
                    //SS_Log::Log( ' member not logged ', SS_Log::INFO);
                    //@TODO : $this->owner->SetSessionMessage("Cette fonctionnalité est réservée aux utilisateurices connecté.e.s", "bad");
            } 
	    }
    }

    public function getMatchingList($currentMemberMatchingCode){

        $list = array();
        $list[] = $currentMemberMatchingCode;

        for ($i = 0; $i < strlen($currentMemberMatchingCode); $i++) {
            $newElement1 = $currentMemberMatchingCode;
            $newElement1[$i] = "%";
            $list[] = $newElement1;
            for ($j = 0; $j < strlen($currentMemberMatchingCode); $j++) {
                $newElement2 = $newElement1;
                $newElement2[$j] = "%";
                $list[] = $newElement2;
            }
        }
        //SS_Log::Log( 'MATCHING LIST ' . json_encode($list) , SS_Log::INFO);
        return $list;
    }

    public static function IOPMatchingTotal()
    {
        return MembersPage::get()->sum('IOPMatchingCount');
    }
}
