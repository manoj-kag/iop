<?php

/**
 * IOPResourcePage
 *
 * @author Johann
 * @extends MlbcResourcePage
 */
class IOPResourcePage extends DataExtension
{
    const IOPAim = 'IOPAim';
    const IOPDuration = 'IOPDuration';
    const IOPCount = 'IOPCount';

    /**
     * @see FilterPageExtension.listFilters
     */
    public function updateListFilters(&$filters)
    {
        $filters = array_merge($filters, array(
            IOPResourcePage::IOPAim,
            IOPResourcePage::IOPDuration,
            IOPResourcePage::IOPCount,
            FilterForm::FULLSEARCH,
        ));
    }
}

/**
 * IOPResourcePage_Controller
 *
 * @author Johann
 * @extends MlbcResourcePage_Controller
 */
class IOPResourcePage_Controller extends Extension
{
    /**
     * @see FilterPageExtension_Controller.FilterForm
     */
    public function updateFilterForm($key, $form)
    {
        if (IOPResourcePage::IOPAim == $key) {
            $form->createDropFilter($key, "Objectif", IOPResource::listIOPAims());
            $form->setCallback($key, function($list, $values) {
                if (!empty($values)) {
                    $list = $list->filter('IOPAim', $values);
                }
                return $list;
            });
        }

        if (IOPResourcePage::IOPDuration == $key) {
            $form->createDropFilter($key, "Temps à consacrer", IOPResource::listIOPDuration());
            $form->setCallback($key, function($list, $values) {
                if (!empty($values)) {
                    $list = $list->filter('IOPDuration', $values);
                }
                return $list;
            });
        }

        if (IOPResourcePage::IOPCount == $key) {
            $form->createFilter($key, "Nombre de participants", IOPResource::listIOPCount());
            $form->setCallback($key, function($list, $values) {
                if (!empty($values)) {
                    $list = $list->filterAny('IOPCount:PartialMatch', $values);
                    // SS_Log::Log(__FUNCTION__.'('. json_encode($values) .') >> '. $list->sql(), SS_Log::INFO);
                }
                return $list;
            });
        }
    }
}
