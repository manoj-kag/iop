<?php

/**
 * IOPCommunityNews
 *
 * @author Johann
 * @extends CommunityNews
 */
class IOPCommunityNews extends DataExtension
{
    private static $db = array(
        'IOPType' => 'Enum("News,Issue,Group,Idea","News")',
    );

    public static function listIOPSimpleTypes($key = null)
    {
        $results = [
            'News'  => '<i class="uk-icon-newspaper-o"></i>&nbsp;Actualité',
            'Issue' => '<i class="uk-icon-question-circle"></i>&nbsp;Aidez-moi',
            'Idea'  => "Idée ou projet",
            'Group' => "Groupe",
        ];
        if ($key) {
            return $results[$key];
        }
        return $results;
    }

    public static function listIOPColorTypes($key = null)
    {
        $results = [
            'News'  => "purple",
            'Issue' => "red",
            'Idea'  => "green",
            'Group' => "blue",
        ];
        if ($key) {
            return $results[$key];
        }
        return $results;
    }

    public static function listIOPTypes()
    {
        return array(
            'News'  => [
                'title' => '<i class="uk-icon-newspaper-o"></i>&nbsp;Actualité',
                'color' => 'purple',
                'description' => "Sur mon activité, mon organisation, sur une collaboration au sein d’IOP, sur une expérience que je souhaite partager.<br/>Rendez-vous dans la section Ressources pour partager d’autres types de news qui peuvent intéresser les autres !"],
            'Issue' => [
                'title' => '<i class="uk-icon-question-circle"></i>&nbsp;Aidez-moi',
                'color' => 'red',
                'description' => "De l’aide ponctuelle pour résoudre un problème, faire avancer un projet, recruter etc."],
            'Idea'  => [
                'title' => '<i class="uk-icon-lightbulb-o"></i>&nbsp;Idée / Projet',
                'color' => 'green',
                'description' => "Dont la réussite dépendra de la diversité des compétences et intérêts réunis"],
            'Group' => [
                'title' => '<i class="uk-icon-group"></i>&nbsp;Groupe d‘échange',
                'color' => 'blue',
                'description' => "Échanges orientés réflexion dans un premier temps. Vous pouvez créer le groupe d’échange avant de publier cette actualité et inclure le lien dans ce formulaire, ou créer le groupe d’échange une fois que vous aurez identifié des personnes motivées à vous rejoindre.<br/>Si vous avez déjà une idée très précise d’événement / atelier que vous souhaitez organiser (objectifs, date, localisation), rendez-vous dans la section Agenda pour le créer !<br/>Votre événement sera automatiquement visible dans le fil d’actualités."],
        );
    }

    public function updateCMSFields(FieldList $fields)
    {
	}

    public function updateSummaryFields(&$fields)
    {
    }
	
    public function onBeforeWrite()
    {
    }
}
