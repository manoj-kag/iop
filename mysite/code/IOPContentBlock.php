<?php

/**
 * IOPContentBlock
 * 
 * @extends ContentBlock
 * @extends PageBlockContent
 */
class IOPContentBlock extends DataExtension
{
    public function updateCMSCodes(&$codes)
    {
        $codes[] = "TotalIOPMatchingCount";
    }
}