#!/bin/bash

echo "options: $@"

clean_only=false
force_staging=false
if [ ! -z $1 ]; then
    if [ $1 = "-c" ] || [ $1 = "--clean" ]; then
        echo "Clean only !"
        clean_only=true
    fi
    if [ $1 = "-f" ] || [ $1 = "--force-staging" ]; then
        echo "Force Staging Silverstripe base !"
        force_staging=true
    fi
fi

SS_DIR=$SS_BASE_DIR
if [ -z "$SS_DIR" ] || [ "$force_staging" = true ]; then
    SS_DIR="/srv/users/serverpilot/silverstripe-base-staging"
fi
echo "Silverstripe base directory is:" $SS_DIR
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "Working directory is:" $DIR

# Test config files exists.
if [ ! -f $DIR/deploy.conf ]; then
    echo $DIR/deploy.conf "is missing !"
fi
if [ ! -f $DIR/letsco.conf ]; then
    echo $DIR/letsco.conf "is missing !"
fi

# Remove previous used links.
find . -type l | while read line
do
    echo "remove" $DIR/$line
    rm $DIR/$line
done

# Add links to ss base modules (deploy.conf)
cat $DIR/deploy.conf | while read line
do
    if [ "$clean_only" != true ]; then
        echo "base links to" $SS_DIR/$line
        ln -s $SS_DIR/$line $DIR
    fi
done

# Add links to custom modules (letsco.conf)
cat $DIR/letsco.conf | while read line
do
    if [ "$clean_only" != true ]; then
        echo "custom links to" $SS_DIR/$line
        ln -s $SS_DIR/$line $DIR
    fi
done
